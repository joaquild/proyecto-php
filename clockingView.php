<style>
    img{
        max-width:90px;
    }
</style>
<style>
    input{
        width: 100%;
    }
    .form-group{
        margin-bottom: 15px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <br/>
        <ol class="breadcrumb">
            <li ><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class=""><a href="/employee">Empleados</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <style>
                            .padre {
                                display: flex;
                                align-items: center;
                            }
                        </style>
                        <div class="row padre">
                            <div class="col-sm-6 ">
                                <h2> <b>Control de fichajes del <b><?php echo date("d-m-Y "); ?></b> </h2>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method="post" enctype="multipart/form-data" >
                        <?php if($haveSchedule = 0){
                            echo "El usuario no tiene un horario definido para hoy";
                            die();
                        } else { ?>
                        <?php if(!empty($scheduled_day_shift_start)){ ?>
                            <div class="col-md-5">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Turno de mañana </h3>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Horario de entrada<br><?php echo $scheduled_day_shift_start; ?></label>
                                            <?php if(!empty($actual_day_shift_start)){ echo "disabled"; }?> type="button" class="btn btn-primary" value="Entrada" id="day-shift-start" onclick="setDayShiftStart(<?php echo "'".$day_shift_start."'"?>, <?php echo "'".$userId."'"  ?>)">
                                            <button <?php if(!empty($actual_day_shift_start)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="day-shift-start" id="day-shift-start">Entrada</button>
                                            <?php echo $actual_day_shift_start; ?>
                                        </div>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Horario de salida<br><?php echo $scheduled_day_shift_end; ?></label>
                                            <button <?php if(empty($actual_day_shift_start) || !empty($actual_day_shift_end)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="day-shift-end" id="day-shift-end">Salida</button>
                                            <?php echo $actual_day_shift_end; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php if(!empty($scheduled_late_shift_start)){ ?>
                            <div class="col-md-5">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Turno de tarde </h3>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Horario de entradaa<br><?php echo  $scheduled_late_shift_start; ?></label>
                                            <button <?php if(empty($actual_day_shift_end) || !empty($actual_late_shift_start)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="late-shift-start" id="late-shift-start">Entrada</button>
                                        </div>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Horario de salida<br><?php echo  $scheduled_late_shift_end; ?></label>
                                            <button <?php if(empty($actual_late_shift_start) || !empty($actual_late_shift_end)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="late-shift-end" id="late-shift-end">Salida</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                            <div class="col-md-2">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Pausa </h3>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Hora inicio<br><br></label>
                                            <button <?php if(!empty($actual_late_shift_end) || empty($actual_day_shift_start)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="pause-start" id="pause-start">Inicio</button>
                                        </div>
                                    </div>
                                    <div class="box-body col-md-6">
                                        <div class="form-group" style="text-align: center;">
                                            <label>Hora fin<br><br></label>
                                            <button <?php if(!empty($actual_late_shift_end) || empty($actual_day_shift_start)){ echo "disabled"; }?> type="submit" class="btn btn-block btn-primary" name="submit" value="pause-end" id="pause-end">Fin</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function setDayShiftStart(scheduled, uid){
        $("#day-shift-start").attr("disabled", "disabled");
        $.ajax({
            url: '/view/ajax/ajax_setDayShiftStart_clocking.php',
            type: "POST",
            data: {
                scheduled: scheduled,
                uid: uid,
            },
            cache: false,
            success: function(dataResult){
                var dataResult = JSON.parse(dataResult);
                if(dataResult.statusCode==200){
                    $("#day-shift-end").removeAttr("disabled");
                }
                else if(dataResult.statusCode==201){
                    alert("Error occured !");
                }
            }
        });
    }}
</script>