<?php
  
    require_once('db/db.php');

    class addCustomerModel{

        private $db;

        public function __construct() {
            $this->db = db::Connection();
        }

        public function addCustomerData($relative, $gender, $name, $lastname1, $lastname2, $id_card, $email, $phone, $birthdate){
            $message = NULL;

            if(!$stmt = $this->db->prepare("INSERT INTO `customer` SET `relative` = ?, `gender` = ?, `name` = ?, `lastname1` = ?, `lastname2` = ?, `id_card` = ?, `email` = ?, `phone` = ?, `birthdate` = ?, `address` = ?, `zipcode` = ?, `location` = ?, `city` = ?, `employed` = ?, `job` = ?, `civil_status` = ?, `dependent_children` = ?, `deleted` = 0, creation_date=NOW() ;")){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("sssssssisssssissiii", $relative, $gender, $name, $lastname1, $lastname2, $id_card, $email, $phone, $birthdate, $address, $zipcode, $location, $city, $employed, $job, $civil_status, $dependent_children)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message .= $stmt->error. "<br />";
                    } else {
                        $last_insert_id = $stmt->insert_id;
                    }
                    $stmt->close();
                    $this->db->close();
                }
            }
            return array($message, $last_insert_id);
        }
    }
?>
