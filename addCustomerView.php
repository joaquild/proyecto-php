<style>
    img{
        max-width:90px;
    }
    label{
        width: 100%;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <br/>
        <ol class="breadcrumb">
            <li ><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class=""><a href="/customer">Clientes</a></li>
            <li class="active"><a href="/customer">Añadir</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h2> <a href="/customer"><i class="fa fa-angle-left white"></i></a>
                            Añadir <b>Cliente</b>
                        </h2>
                    </div>
                    <div class="box-body">
                        <form role="form" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="inputEmail4">Familiar o relacionado</label>
                                    <input type="text" class="form-control" id="relative" name="relative" value=""">
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="inputPassword4">Género</label>
                                    <input type="text" class="form-control" id="gender" name="gender" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputPassword4">Nombre</label>
                                    <input  type="text" required class="form-control" id="name" name="name" value="" >
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputPassword4">Primer apellido</label>
                                    <input type="text" required class="form-control" id="lastname1" name="lastname1" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputPassword4">Segundo apellido</label>
                                    <input type="text" class="form-control" id="lastname2" name="lastname2" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputPassword4">DNI/NIE</label>
                                    <input type="text" required class="form-control" id="id_card" name="id_card" value="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputAddress">Correo electronico</label>
                                    <input type="text" class="form-control" id="email" name="email" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputAddress">Teléfono principal</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputAddress">Fecha de nacimiento (YY-MM-DD)</label>
                                    <input type="text" class="form-control" id="birthdate" name="birthdate" value="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputAddress">Dirección fiscal</label>
                                    <input type="text" class="form-control" id="address" name="address" value="">
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="inputAddress">Código Postal</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" value="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputAddress">Localidad</label>
                                    <input type="text" class="form-control" id="location" name="location" value="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="inputAddress">Provincia</label>
                                    <input type="text" class="form-control" id="city" name="city" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="type">Situación Laboral</label>
                                    <select class="form-control" id="employed" name="employed" onchange="jobStatus()" required>
                                        <option value="">¿Está trabajando?</option>
                                        <option value="1">Trabaja</option>
                                        <option value="0">En paro</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputAddress">Profesión</label>
                                    <input type="text" class="form-control" id="job" name="job" value="">
                                </div>

                                <script>
                                function jobStatus(){
                                    if(document.getElementById("employed").value == 0){
                                        $('#job').prop("disabled", true);
                                    } else {
                                        $('#job').prop("disabled", false);
                                    }
                                }
                                </script>
                                <div class="form-group col-md-2">
                                    <label for="inputAddress">Estado civil</label>
                                    <select class="form-control" id="civil_status" name="civil_status" required>
                                        <option value="">¿Cual es el estado civil?</option>
                                        <option value="C">Casado</option>
                                        <option value="S">Soltero</option>
                                        <option value="D">Divorciado</option>
                                        <option value="V">Viudo</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="inputAddress">Hijos a cargo</label>
                                    <input type="text" class="form-control" id="dependent_children" name="dependent_children" value="">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right" name="submit" value="ok">Añadir cliente</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
