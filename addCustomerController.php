<?php
 
require_once('model/customer/addCustomerModel.php');
require_once ('controller/customer/mail.php');

$main = new addCustomerModel();


if(!isset($_POST['submit'])){
    require_once('view/customer/addCustomerView.php');
}
else {
    $relative = $_POST['relative'];
    $gender = $_POST['gender'];
    $name = $_POST['name'];
    $lastname1 = $_POST['lastname1'];
    $lastname2 = $_POST['lastname2'];
    $id_card = $_POST['id_card'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    $zipcode = $_POST['zipcode'];
    $location = $_POST['location'];
    $city = $_POST['city'];
    $employed = $_POST['employed'];
    $job = $_POST['job'];
    $civil_status = $_POST['civil_status'];
    $dependent_children = $_POST['dependent_children'];


    list($message, $last_insert_id) = $main->addCustomerData($relative, $gender, $name, $lastname1, $lastname2, $id_card, $email, $phone, $birthdate, $address, $zipcode, $location, $city, $employed, $job, $civil_status, $dependent_children);
    }


?>