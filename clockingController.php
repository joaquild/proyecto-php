<?php

require_once('model/employee/clocking/clockingModel.php');
   
$main = new clockingModel();

$userId = $_SESSION['uid'];
list($message, $scheduled) = $main->getScheduledClocking($userId);
list($message, $actual) = $main->getActualClocking($userId);

if($scheduled) {
    $haveSchedule = 1;
    $scheduled_day_shift_start = $scheduled[0]['day_shift_start'];
    $scheduled_day_shift_end = $scheduled[0]['day_shift_end'];
    $scheduled_late_shift_start = $scheduled[0]['late_shift_start'];
    $scheduled_late_shift_end = $scheduled[0]['late_shift_end'];
} else {
    $haveSchedule = 0;
}

if(!empty($actual)){
    $haveActual = 1;
    $actual_day_shift_start = $actual[0]['actual_day_shift_start'];
    $actual_day_shift_end = $actual[0]['actual_day_shift_end'];
    $actual_late_shift_start = $actual[0]['actual_late_shift_start'];
    $actual_late_shift_end = $actual[0]['actual_late_shift_end'];
    $clocking_id = $actual[0]['clocking_id'];
} else {
    $haveActual = 0;
}

if(isset($_POST['submit'])) {
    if($haveActual == 0){
        list($message, $actual) = $main->setclocking($userId, $scheduled, $_POST['submit']);
    }
    if($haveActual == 1){
        if($_POST['submit'] == 'pause-start' || $_POST['submit'] == 'pause-end'){
        list($message, $actual) = $main->setClockingHistory($_POST['submit'], $userId, $clocking_id);
        }
    else{
        list($message, $actual) = $main->updateClocking($_POST['submit'], $clocking_id, $scheduled, $userId);
    }
    }
    unset($_POST);
    echo "<script>window.location.href='/employee/clocking'</script>";
}
else {
    require_once('view/employee/clocking/clockingView.php');
}

?>
