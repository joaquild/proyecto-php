<div class="content-wrapper">
<section class="content-header">
    <br/>
    <ol class="breadcrumb">
        <li ><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="/employee/absence">Ausencias</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <style>
                        .padre {
                            display: flex;
                            align-items: center;
                        }
                    </style>
                    <div class="row padre">
                        <div class="col-sm-12">
                            <h2>Ausencias <b> Empleados</b></h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TODO: Hacer que este filtro de fechas funcione (ahora es Ãºnicamente vista) -->
            <div class="box-body">
                <div class="box-body table-responsive">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Fecha inicial</label>
                        <div class="input-group date" data-provide="datepicker" >
                            <input type="text" class="form-control pull-right" class="datepicker" id="initialdate" name="initialdate" >
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Fecha final</label>
                        <div class="input-group date" data-provide="datepicker" >
                            <input type="text" class="form-control pull-right" class="datepicker" id="initialdate" name="initialdate" >
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pull-right">
                    <button type="button" class=" btn btn-block btn-primary" >Filtrar</button>
                </div>
                <table class="table table-striped table-hover" id="employee-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Turno</th>
                            <th>DÃ­a</th>
                            <th>Entrada prevista</th>
                            <th>Entrada real</th>
                            <th>Justificada</th>
                            <th>Motivo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if (empty($dateByUser) and empty($absenceByUser)){
                            echo "<tr>";
                            echo "<td>Es un empleado decente</td>";
                            echo "</tr>";}
                        else{
                            if (empty($dateByUser)){
                                echo "<tr>";
                                echo "<td>Hoy no llego tarde</td>";
                                echo "</tr>";
                            }else {
                                foreach ($dateByUser as $value) {
                                    echo "<tr>";
                                    echo "<td>Retraso</td>";
                                    echo "<td>" . $value['franja'] . "</td>";
                                    echo "<td>" . $value['dia'] . "</td>";
                                    echo "<td>" . $value['dayprevista'] . "</td>";
                                    echo "<td>" . $value['dayreal'] . "</td>";
                                    echo "<td><i style=\"color: red\" class=\"material-icons\" data-toggle=\"tooltip\" title=\"Eliminar\">&#xe14c;</i></td>";
                                    echo "<td></td>";
                                    echo "<td>";
                                    echo "<a href=\"/employee/absence/justify/\"  class=\"edit \" ><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Editar\">&#xe254;</i></a>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                            }if (empty($absenceByUser)){
                                echo "<tr>";
                                echo "<td>Ayer no falto</td>";
                                echo "</tr>";

                            }else {
                                foreach ($absenceByUser as $value) {
                                    echo "<tr>";
                                    echo "<td>Ausencia</td>";
                                    echo "<td>" . $value['franja'] . "</td>";
                                    echo "<td>" . $value['dia'] . "</td>";
                                    echo "<td>" . $value['dayprevista'] . "</td>";
                                    echo "<td>" . $value['dayreal'] . "</td>";
                                    echo "<td><i style=\"color: red\" class=\"material-icons\" data-toggle=\"tooltip\" title=\"Eliminar\">&#xe14c;</i></td>";
                                    echo "<td></td>";
                                    echo "<td>";
                                    echo "<a href=\"/employee/absence/justify/\"  class=\"edit \" ><i class=\"material-icons\" data-toggle=\"tooltip\" title=\"Editar\">&#xe254;</i></a>";
                                    echo "</td>";
                                    echo "</tr>";
                            }}
                        }?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>