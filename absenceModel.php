<?php

    require_once('db/db.php');

    class absenceModel{

        private $db;

        public function __construct() {
            $this->db = db::Connection();
        }
    
        public function getDelayByUser($var1) { // TODO: Mirar que únicamente se seleccionen copmaías a las que tiene acceso el usuario
            $query=$this->db->query( "SELECT * FROM
                (SELECT 'Mañana' as franja, c.user_id,
                DATE(actual_day_shift_start) as dia,
                scheduled_day_shift_start as dayprevista,
                TIME(actual_day_shift_start) as dayreal,
                TIMEDIFF(time(actual_day_shift_start), scheduled_day_shift_start) as dayretraso
                FROM preico_crm.clocking as c
                JOIN user u on c.user_id =u.user_id
                UNION ALL
                SELECT 'Tarde' as franja, c.user_id,
                DATE(actual_late_shift_start) as diatarde,
                scheduled_late_shift_start as tardeprevista,
                TIME(actual_late_shift_start) as tardereal,
                TIMEDIFF(time(actual_late_shift_start), scheduled_late_shift_start) as tarderetraso
                FROM preico_crm.clocking as c
                JOIN user u on c.user_id =u.user_id) delay
                where dayretraso > '00:15:00'
                AND  dia = CURDATE();");
            if ($query->num_rows > 0) {
                while ($filas = $query->fetch_assoc()) {
                    $this->date[] = $filas;
                }
                return $this->date;
            }
            else{
                $this->date = NULL;
            }
        }

        public function getAbsenceByUser($var1) { // TODO: Mirar que únicamente se seleccionen copmaías a las que tiene acceso el usuario
            $query=$this->db->query( "SELECT * FROM
                (SELECT 'Mañana' as franja, c.user_id,
                DATE_SUB(CURDATE(), INTERVAL 1 DAY) as ayer,
                DATE(actual_day_shift_start) as dia,
                scheduled_day_shift_start as dayprevista,
                TIME(actual_day_shift_start) as dayreal,
                TIMEDIFF(time(actual_day_shift_start), scheduled_day_shift_start) as dayretraso
                FROM preico_crm.clocking as c
                JOIN user u on c.user_id =u.user_id
                UNION ALL
                SELECT 'Tarde' as franja, c.user_id,
                DATE_SUB(CURDATE(), INTERVAL 1 DAY) as ayer,
                DATE(actual_late_shift_start) as diatarde,
                scheduled_late_shift_start as tardeprevista,
                TIME(actual_late_shift_start) as tardereal,
                TIMEDIFF(time(actual_late_shift_start), scheduled_late_shift_start) as tarderetraso
                FROM preico_crm.clocking as c
                JOIN user u on c.user_id =u.user_id) delay
                where dia = ayer;");
            if ($query->num_rows > 0) {
                while ($filas = $query->fetch_assoc()) {
                    $this->date[] = $filas;
                }
                return $this->date;
            }
            else{
                $this->date = NULL;
            }
        }
    }
?>
