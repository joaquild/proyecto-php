<?php
  
    require_once('db/db.php');

    class clockingModel{

        private $db;

        public function __construct() {
            $this->db = db::Connection();
        }

        public function getScheduledClocking($uid) {
            $message = NULL;
            if(!$stmt = $this->db->prepare("SELECT e.employee_id, user_id, day_shift_start, day_shift_end, late_shift_start, late_shift_end 
                                                    FROM employee_work_data ewd
                                                    join employee e on ewd.employee_id = e.employee_id
                                                    where e.user_id = ?
                                                    ;")){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("i", $uid)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message .= $stmt->error. "<br />";
                    } else {
                        $result = $stmt->get_result();
                        $result = $result->fetch_all(MYSQLI_ASSOC);
                        $stmt->close();
                    }
                }
            }
            return array($message, $result);
        }

        public function getActualClocking($uid) {
            $message = NULL;
            if(!$stmt = $this->db->prepare("SELECT clocking_id, actual_day_shift_start, actual_day_shift_end, actual_late_shift_start, actual_late_shift_end 
                                                    FROM clocking c
                                                    where c.user_id = ?
                                                    and date(actual_day_shift_start) = date(now())
                                                    or date(actual_day_shift_end) = date(now())
                                                    or date(actual_late_shift_start) = date(now())
                                                    or date(actual_late_shift_end) = date(now())            
                                                    ;")){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("i", $uid)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message .= $stmt->error. "<br />";
                    } else {
                        $result = $stmt->get_result();
                        $result = $result->fetch_all(MYSQLI_ASSOC);
                        $stmt->close();
                    }
                }
            }
            return array($message, $result);
        }

        public function updateClocking($type, $clocking_id, $scheduled, $uid){
            $message = NULL;
            if($type == 'day-shift-start'){
                $clockingDate = $scheduled[0]['day_shift_start'];
                $query = "UPDATE  `clocking` set `scheduled_day_shift_start` = ?, `actual_day_shift_start` = NOW() WHERE clocking_id = ?;";
            }
            if($type == 'day-shift-end'){
                $clockingDate = $scheduled[0]['day_shift_end'];
                $query = "UPDATE  `clocking` set `scheduled_day_shift_end` = ?, `actual_day_shift_end` = NOW() WHERE clocking_id = ?;";
            }
            if($type == 'late-shift-start'){
                $clockingDate = $scheduled[0]['day_shift_start'];
                $query = "UPDATE  `clocking` set `scheduled_late_shift_start` = ?, `actual_late_shift_start` = NOW() WHERE clocking_id = ?;";
            }
            if($type == 'late-shift-end'){
                $clockingDate = $scheduled[0]['day_shift_end'];
                $query = "UPDATE  `clocking` set `scheduled_late_shift_end` = ?, `actual_late_shift_end` = NOW() WHERE clocking_id = ?;";
            }

            if(!$stmt = $this->db->prepare($query)){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("si", $clockingDate, $clocking_id)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message -= $stmt->error . "<br />";
                    } else{
                        $stmt->close();
                        $query = "INSERT INTO `clocking_history` (`timestamp`, `type`, `user_id`, `clocking_id`) VALUES (now(), ?, ?, ?);";
                        if(!$stmt = $this->db->prepare($query)){
                            $message .= $this->db->error. "<br />";
                        } else {
                            if(!$stmt->bind_param("sii", $type, $uid, $clocking_id)){
                                $message .= $stmt->error. "<br />";
                            } else {
                                if(!$stmt->execute()){
                                    $message -= $stmt->error . "<br />";
                                } else{
                                    $stmt->close();
                                    return array($message, $clocking_id);
                                }
                            }
                        }
                    }
                }
            }
        }
        public function setclocking($uid, $scheduled, $type) {
            $message = NULL;
            if($type == 'day-shift-start'){
                $clockingDate = $scheduled[0]['day_shift_start'];
                $query = "INSERT INTO `clocking` (`user_id`, `scheduled_day_shift_start`, `actual_day_shift_start`) VALUES (?, ?, now());";
            }
            if($type == 'day-shift-end'){
                $clockingDate = $scheduled[0]['day_shift_end'];
                $query = "INSERT INTO `clocking` (`user_id`, `scheduled_day_shift_end`, `actual_day_shift_end`) VALUES (?, ?, now());";
            }
            if($type == 'late-shift-start'){
                $clockingDate = $scheduled[0]['day_shift_start'];
                $query = "INSERT INTO `clocking` (`user_id`, `scheduled_late_shift_start`, `actual_late_shift_start`) VALUES (?, ?, now());";
            }
            if($type == 'late-shift-end'){
                $clockingDate = $scheduled[0]['day_shift_end'];
                $query = "INSERT INTO `clocking` (`user_id`, `scheduled_late_shift_end`, `actual_late_shift_end`) VALUES (?, ?, now());";
            }

            if(!$stmt = $this->db->prepare($query)){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("is", $uid, $clockingDate)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message -= $stmt->error . "<br />";
                    } else{
                        $insert_id = $stmt->insert_id;

                        $query = "INSERT INTO `clocking_history` (`timestamp`, `type`, `user_id`, `clocking_id`) VALUES (now(), ?, ?, ?);";

                            if(!$stmt = $this->db->prepare($query)){
                                $message .= $this->db->error. "<br />";
                            } else{
                            if(!$stmt->bind_param("sii", $type, $uid, $insert_id)){
                                $message .= $stmt->error. "<br />";
                            } else {
                                if(!$stmt->execute()){
                                    $message -= $stmt->error . "<br />";
                                } else{
                                    $stmt->close();
                                    return array($message, $insert_id);
                                }
                            }
                        }
                    }
                }
            }
        }

        public function setClockingHistory($type, $userId, $clocking_id) {
            $message = NULL;
            if($type == 'pause-start'){
                $query = "INSERT INTO  `clocking_history` (`timestamp`, `type`, `user_id`, `clocking_id` )VALUES (NOW(), ?, ?, ?);";
            }
            if($type == 'pause-end'){
                $query = "INSERT INTO  `clocking_history` (`timestamp`, `type`, `user_id`, `clocking_id` )VALUES (NOW(), ?, ?, ?);";
            }

            if(!$stmt = $this->db->prepare($query)){
                $message .= $this->db->error. "<br />";
            } else {
                if(!$stmt->bind_param("sii", $_POST['submit'], $userId, $clocking_id)){
                    $message .= $stmt->error. "<br />";
                } else {
                    if(!$stmt->execute()){
                        $message -= $stmt->error . "<br />";
                    } else{
                        $insert_id = $stmt->insert_id;
                    }
                }
            }
        }
    }
?>
